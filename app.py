import sys

from flask import jsonify, request
from sqlalchemy.exc import IntegrityError

from config import app
from utils.manager import ManagerContacts
from logger import logger

MSG = {"info": "an error occurred"}
WARNING = "Exception: \n{}"

with app.app_context():
    mc = ManagerContacts()


@app.route('/contacts', methods=["GET"])
def contact_manager_list_all():
    try:
        resp = mc.get_all_contacts()
    except Exception as e:
        logger.warning(msg=WARNING.format(e))
        resp = MSG

    return jsonify(resp)


@app.route('/find_contact/<contact_id>', methods=["GET"])
def contact_manager_find(contact_id):
    """
    Find a contact using contact unique identifier
    :param contact_id: str
    :return: json
    """
    try:
        resp = mc.get_contact_detail(contact_id=contact_id)
    except IntegrityError as e:
        logger.warning(msg=WARNING.format(e))
        resp = MSG

    return jsonify(resp)


@app.route('/create_contact', methods=["POST"])
def contact_manager_create():
    """
    Create a new contact using json data containing firstname, lastname, username and gender
    :return: json
    """
    data = request.json

    try:
        resp = mc.add_new_contact(data)
    except IntegrityError as e:
        logger.warning(msg=WARNING.format(e))
        resp = MSG

    return jsonify(resp)


@app.route('/update_contact/<contact_id>', methods=["PUT"])
def contact_manager_update(contact_id):
    """
    update a contact using contact_id and data to update
    :param contact_id: str
    :return: json
    """
    data = request.json

    try:
        resp = mc.update_contact(contact_id=contact_id, data=data)
    except IntegrityError as e:
        logger.warning(msg=WARNING.format(e))
        resp = MSG

    return jsonify(resp)


@app.route('/remove_contact', methods=["DELETE"])
def contact_manager_delete():
    """
    Delete all stored data older than 60 seconds
    :return: json
    """
    try:
        resp = mc.delete_contact()
        resp = {"info": "{} contacts was deleted".format(resp)}
    except Exception as e:
        logger.warning(msg=WARNING.format(e))
        resp = MSG

    return jsonify(resp)


if __name__ == "__main__":
    app.run()
