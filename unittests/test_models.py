from utils.models import Contacts, Email


def test_new_contact():
    contact = Contacts("John", "Johnson", "Roy", "male")
    assert contact.id is None
    assert contact.username == "John"
    assert contact.firstname == "Johnson"
    assert contact.lastname == "Roy"
    assert contact.gender == "male"


def test_new_email():
    email = Email(address="John@john.com")
    assert email.id is None
    assert email.contacts_id is None
    assert email.address == "John@john.com"
