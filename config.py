import os
from datetime import timedelta

from celery import Celery
from celery.schedules import crontab

from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))

redis_uri = "redis://localhost:6379/0"

# Configure the SQLAlchemy
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'database/contact.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Create the SQLAlchemy db instance
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Initialize Marshmallow
ma = Marshmallow(app)


def make_celery(app):
    """
    Celery configuration
    :param app: flask app instance
    :return:
    """
    app.config['CELERY_BROKER_URL'] = redis_uri
    app.config['CELERY_RESULT_BACKEND'] = redis_uri
    app.config['CELERYBEAT_SCHEDULE'] = {
        # Executes every 15 second
        'create_contact_task-every-fifteen-second': {
            'task': 'create_contact_task',
            'schedule': timedelta(seconds=15)
        },
        # Executes every minute
        'delete_contact_task-every-minute': {
            'task': 'delete_contact_task',
            'schedule': crontab(minute="*")
        }
    }

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])

    celery.conf.update(app.config)

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery


celery = make_celery(app=app)
