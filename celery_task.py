import time
import string
import requests
from random import choice
from config import celery
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


def name_generator():
    """Generate a random"""
    return ''.join(choice(string.ascii_letters) for _ in range(6))


@celery.task(name="delete_contact_task")
def delete_contact_task():
    resp = requests.delete("http://127.0.0.1:5000/remove_contact")
    logger.info(resp.json())


@celery.task(name="create_contact_task")
def create_contact_task():
    firstname = name_generator()
    lastname = name_generator()
    username = firstname + str(time.time()).split(".")[0]
    data = {"username": username,
            "firstname": firstname,
            "lastname": lastname,
            "gender": "female",
            "emails": [username+"@gmail.com", username+"@hotmail.com"]
            }
    resp = requests.post("http://127.0.0.1:5000/create_contact", json=data)
    logger.info(resp.json())
