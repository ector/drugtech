# DrugDevTechTest

This file contains the solution to the questions given

### Prerequisites

Ensure you have redis installed and running\
If you have docker installed to start redis run the command below

```
docker run --name redis -p 6379:6379 -e ALLOW_EMPTY_PASSWORD=yes bitnami/redis:latest
```

### Installing
In the project directory, create a virtualenv and run   

```
pip install -r requirements.txt
celery -A celery_task.celery worker --beat --loglevel=info
```
and run the flask app

```
export FLASK_APP=app
export FLASK_ENV=development
flask run
```

## Authors

* **Tola** - [DrugTech](https://bitbucket.org/ector/drugtech)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
