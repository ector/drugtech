from datetime import datetime

from config import ma, db

default_len = 32


class Contacts(db.Model):
    """
    Contacts model
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(default_len), unique=True)
    firstname = db.Column(db.String(default_len))
    lastname = db.Column(db.String(default_len))
    gender = db.Column(db.String)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    addresses = db.relationship('Email', backref="contact", cascade="all, delete, delete-orphan")

    def __init__(self, username, firstname, lastname, gender):
        self.username = username
        self.firstname = firstname
        self.lastname = lastname
        self.gender = gender


class Email(db.Model):
    """
    Email model
    """
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(default_len), unique=True)
    contacts_id = db.Column(db.Integer, db.ForeignKey("contacts.id"), nullable=False)

    def __init__(self, address):
        self.address = address


class EmailSchema(ma.ModelSchema):
    class Meta:
        fields = ["address"]


class ContactSchema(ma.ModelSchema):
    addresses = ma.Nested(EmailSchema, many=True)

    class Meta:
        fields = ["id", "username", "firstname", "lastname", "gender", "addresses"]


db.create_all()

contact_schema = ContactSchema()
contacts_schema = ContactSchema(many=True)
