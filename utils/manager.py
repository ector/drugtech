from datetime import datetime, timedelta

from utils.models import (Contacts, db, contact_schema, contacts_schema, Email)


class ManagerContacts(object):
    def __init__(self):
        self.db = db
        self.contact = Contacts
        self.email = Email

    def add_new_contact(self, data):
        """
        Add new data using the data object
        :param data: dict
        :return: dict
        """
        username = data.get("username")
        lastname = data.get("lastname")
        firstname = data.get("firstname")
        gender = data.get("gender")
        emails = data.get("emails")

        nc = self.contact(username, firstname, lastname, gender)

        # self.db.session.add(nc)
        for email in emails:
            nc.addresses.append(self.email(address=email))

        # add it to a new session
        self.db.session.add(nc)
        self.db.session.commit()

        return {"info": "New contact added with id {}".format(nc.id)}

    def get_contact_detail(self, contact_id):
        """
        Retrieve the a contact
        :param contact_id: str
        :return: dict
        """
        results = self.contact.query.join(self.email).filter(self.contact.id == int(contact_id)).all()
        return contacts_schema.dump(results)[0]

    def get_all_contacts(self):
        """
        Retrieve all contacts
        :return: dict
        """
        cons = self.contact.query.join(self.email).all()
        contacts = contacts_schema.dump(cons)
        return {"contacts": contacts}

    def update_contact(self, contact_id, data):
        """
        Update a contact
        :param contact_id: str
        :param data: dict
        :return: dict
        """
        contact = self.contact.query.get(int(contact_id))
        for key, val in data.items():
            if key == "username":
                contact.username = val
            elif key == "firstname":
                contact.firstname = val
            elif key == "lastname":
                contact.lastname = val
            elif key == "gender":
                contact.gender = val
            elif key == "emails":
                for email in data.get(key):
                    contact.addresses.append(self.email(address=email))
            else:
                pass
        db.session.commit()
        return contact_schema.dump(contact)

    def delete_contact(self):
        """
        Delete contact
        :return: int
        """
        time_now = datetime.utcnow() - timedelta(seconds=60)
        contact = self.contact.query.filter(self.contact.timestamp <= time_now).all()

        # add it to a new session
        for cont in contact:
            self.db.session.delete(cont)
        self.db.session.commit()
        return len(contact)
