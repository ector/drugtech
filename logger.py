import os
import logging
import logging.config


def make_logger():
    """
    Configure and create a file logger
    :return: logging
    """
    if not os.path.isdir("logs"):
        os.mkdir("logs")

    logging.config.fileConfig(fname='configs/logfile.conf', disable_existing_loggers=False)

    # Get the logger specified in the file
    logger = logging.getLogger(__name__)

    # Create handlers
    f_handler = logging.FileHandler('logs/file.log')
    f_handler.setLevel(logging.WARNING)

    # Create formatters and add it to handlers
    f_format = logging.Formatter('%(asctime)s - %(funcName)s - %(levelname)s - %(message)s')
    f_handler.setFormatter(f_format)

    # Add handlers to the logger
    logger.addHandler(f_handler)

    return logger


logger = make_logger()
